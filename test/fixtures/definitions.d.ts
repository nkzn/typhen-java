declare module Example {
  interface Product {
    id: number;
    name: string;
    price: number;
    tags?: string[];
  }
}
