const fs = require('fs');
const assert = require('power-assert');
const rimraf = require('rimraf');
const glob = require('glob');
const typhen = require('typhen');

describe('typhen-java', () => {
  var expectedFileNames = glob.sync('./test/fixtures/**/*.java');

  before((done) => {
    typhen.logger.level = typhen.logger.LogLevel.Silent;

    rimraf('./.tmp/generated', () => {
      const plugin = typhen.loadPlugin('./lib', {});
      typhen.run({
        plugin: plugin,
        src: 'test/fixtures/definitions.d.ts',
        dest: './.tmp/generated'
      }).done(function() {
        done();
      }, function(e) {
        throw e;
      });
    });
  });

  expectedFileNames.forEach((expectedFileName) => {
    context(expectedFileName.replace('./test/fixtures/', ''), () => {
      it('should generate a Java file', function() {
        var actualFileName = expectedFileName.replace('./test/fixtures', './.tmp/generated');
        assert(fs.readFileSync(actualFileName, 'utf-8') === fs.readFileSync(expectedFileName, 'utf-8'));
      });
    });
  });

  it('should not generate unnecessary files', () => {
    var actualFileNames = glob.sync('./.tmp/generated/**/*.java');
    assert(actualFileNames.length === expectedFileNames.length);
  });
});
