'use strict';

module.exports = function(typhen, options) {
  return typhen.createPlugin({
    pluginDirectory: __dirname,
    rename: function(symbol, name) {
      if (symbol.kind === typhen.SymbolKind.PrimitiveType && name === 'number') {
        return 'double';
      }
      return name;
    },
    generate: function(generator, types, modules) {
      types.forEach(function(type) {
        switch (type.kind) {
          case typhen.SymbolKind.Interface:
          case typhen.SymbolKind.Class:
            generator.generate('templates/class.hbs', 'upperCamelCase:**/*.java', type);
            break;
        }
      });
    }
  });
}
