const gulp = require('gulp');
const mocha = require('gulp-spawn-mocha');

require('intelli-espower-loader');

const DEBUG = process.env.NODE_ENV === 'debug';

gulp.task('test', () => {
  return gulp
    .src(['test/*.test.js'], {read: false})
    .pipe(mocha({
      debugBrk: DEBUG,
      istanbul: !DEBUG
    }));
});
