typhen-java
==========

> Converts TypeScript Interfaces to Java.

License
----------

Copyright (c) 2016 Yukiya Nakagawa Licensed under the MIT license.
